<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die();
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE html>
<html xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<head>
    <?$APPLICATION->ShowHead();?>
    <title><?$APPLICATION->ShowTitle();?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="white">
    <meta name="apple-mobile-web-app-title" content="Beru">
    <link rel="shortcut icon" type="image/x-icon" href="<?=SITE_DIR?>favicon.ico" />
    <link rel="apple-touch-icon" sizes="180x180" href="<?=SITE_DIR?>apple-touch-icon.png">
    <link rel="icon" type="image/png" href="<?=SITE_DIR?>favicon_16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="<?=SITE_DIR?>favicon_32.png" sizes="32x32">
    <meta name="theme-color" content="#ffffff">
    <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css");?>
    <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap-theme.min.css");?>
    <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.min.css");?>
    <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."https://hayageek.github.io/jQuery-Upload-File/4.0.10/uploadfile.css");?>
    <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/styles.css");?>
    <?$APPLICATION->AddHeadScript('https://api-maps.yandex.ru/2.1/?lang=ru_RU');?>
</head>
<body>
<?if (IsModuleInstalled("im")) $APPLICATION->IncludeComponent("bitrix:im.messenger", "", Array(), null, array("HIDE_ICONS" => "Y"));?>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
    <header>

    </header>
